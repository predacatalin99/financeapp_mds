import 'package:flutter/material.dart';
import 'package:financeapp_mds/models/review.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class ReviewChart extends StatelessWidget {
  final String chartName;
  final List<Review> data;
  ReviewChart({this.chartName, this.data});
  @override
  Widget build(BuildContext context) {
    List<charts.Series<Review, String>> series
    = [
      charts.Series(
        id: "Review",
        data: data,
        domainFn: (Review review, _) => review.name,
        measureFn: (Review review, _) => review.amount,
        colorFn: (Review review, _) => review.barColor
      )
    ];
    return Container(
      height: 240,
      width:185,
      padding: EdgeInsets.all(5),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Text(
                chartName,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Expanded(child: charts.BarChart(series, animate: true))
            ],
          ),
        ),
       
      )
    );
  }
}