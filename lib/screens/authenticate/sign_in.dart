import 'package:financeapp_mds/services/auth.dart';
import 'package:financeapp_mds/shared/constants.dart';
import 'package:financeapp_mds/shared/loading.dart';
import 'package:flutter/material.dart';

class SignIn extends StatefulWidget {
  
  final Function toggleView;
  SignIn({ this.toggleView });

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {

  final AuthService _auth = AuthService();
  final  _formKey = GlobalKey<FormState>();
  //text field state
  String email = '';
  String password = '';
  String error = '';
  bool loading = false;
  List<bool> isSelected;

    @override
    void initState() {
        isSelected = [true, false];
        super.initState();
    }


@override
  Widget build(BuildContext context) {
    return loading ? Loading() : Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            boxShadow: [
              new BoxShadow(
                color: Colors.grey[300],
                offset: new Offset(0, 0),
                blurRadius: 15,
                spreadRadius: 10
              )
            ],
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          width: 9 * MediaQuery.of(context).size.width / 10,
          height: 2.4 * MediaQuery.of(context).size.height / 3,
          padding: EdgeInsets.fromLTRB(30, 24, 30, 0),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 36,
                  child: ToggleButtons(
                    fillColor: HexColor("#4AD285"), //culoare buton selectat
                    selectedColor: Colors.white, //culoare text buton selectat
                    color: Colors.grey[400], // culoare text buton neselectat
                    focusColor: Colors.yellow,
                    hoverColor: Colors.red,
                    disabledColor: Colors.blue,
                    disabledBorderColor: Colors.black,
                    borderRadius: BorderRadius.circular(30),
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 0),
                        child: Text(
                          'Login',
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 0),
                        child: Text(
                          'Signup',
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
              ],
              onPressed: (int index) {
                  setState(() {
                  for (int i = 0; i < isSelected.length; i++) {
                      isSelected[i] = i == index;
                      if (isSelected[1])
                        widget.toggleView();
                  }
                  });
                },
                isSelected: isSelected,
              ),
            ),
            
            SizedBox(height:44),
          SizedBox(
            height: 44,
            child: TextFormField(
              errorTextPresent: false,
              decoration: InputDecoration(
                prefixIcon: SizedBox(
                  child: Center(
                    widthFactor: 3.5,
                    child: Image.asset(
                      'assets/email.png',
                      width:16,
                      height:12.18,
                    ),
                  ),
                ),
                  
                hintText: 'Email Address',
                hintStyle:  TextStyle(
                      color: HexColor("#1A1A1A").withOpacity(0.2),
                      fontSize: 14,
                      height: 0.85,

                    ),
                enabledBorder: new OutlineInputBorder(
                borderRadius: new BorderRadius.circular(40.0),
                borderSide: BorderSide(color: HexColor("#1A1A1A").withOpacity(0.2)),
                ),
                
                border: new OutlineInputBorder(
                borderRadius: new BorderRadius.circular(40.0),
                borderSide: BorderSide(color: HexColor("#1A1A1A").withOpacity(0.2)),

                ),
              ),
              validator: (val) {
                if (val.isEmpty){
                  setState(() => error += "Enter an email\n");
                  return '';

                }
                else {
                  return null;
                }
              },

              onChanged: (val) {
                setState(() => email = val);
              },
            ),
          ),
          SizedBox(height: 16.0),
        
          SizedBox(
            height: 44,
            child: TextFormField(
              errorTextPresent: false,
              decoration: InputDecoration(
                prefixIcon: SizedBox(
                  child: Center(
                    widthFactor: 4.0,
                    child: Image.asset(
                      'assets/lock.png',
                      width:14,
                      height:16,
                    ),
                  ),
                ),
                  
                hintText: 'Password',
                hintStyle:  TextStyle(
                      color: HexColor("#1A1A1A").withOpacity(0.2),
                      fontSize: 14,
                      height:0.8,
                      
                    ),
                enabledBorder: new OutlineInputBorder(
                borderRadius: new BorderRadius.circular(40.0),
                borderSide: BorderSide(color: HexColor("#1A1A1A").withOpacity(0.2)),

                ),
                border: new OutlineInputBorder(
                borderRadius: new BorderRadius.circular(40.0),
                borderSide: BorderSide(color: HexColor("#1A1A1A").withOpacity(0.2)),

                ),
              ),
              validator: (val) {
                if (val.length < 6){
                  setState(() => error += 'Enter a password 6+ chars long');
                  return '';

                }
                else {
                  return null;
                }
              },
              
              obscureText: true,
              onChanged: (val) {
                setState(() => password = val);

              }
            ),
          ),
          SizedBox(height: 16),
          SizedBox(
            height: 44,
            width: 130,
            child: RaisedButton(
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(25.0)
              ),
              color: HexColor("#4A69FF"),
              child: Text(
                'LOG IN',
                style: TextStyle(color: Colors.white),
              ),
               onPressed: () async {
                 if (error.length > 0)
              error = '';
          if(_formKey.currentState.validate()){
            
            setState(() => loading = true);
            dynamic result = await _auth.signInWithEmailAndPassword(email, password);
            if(result == null) {
              setState(() {
                loading = false;
                error = 'Could not sign in with those credentials';
              });
            }
          }
        }
            ),
          ),
          SizedBox(height:12.0),
          Text(
            error,
            style: TextStyle(color: Colors.red, fontSize: 14.0),
          )
        ],
              ),
            )
          ),
      )
    );
  }
}
