import 'package:flutter_test/flutter_test.dart';
import 'package:financeapp_mds/services/auth.dart';
import 'package:financeapp_mds/screens/financial_review.dart';
import 'dart:async';
import 'package:financeapp_mds/models/review.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  test('SignInAnon signs in anonymously', () async {
    AuthService _auth = AuthService();
    dynamic result = await _auth.signInAnon();
    expect(result, null);
  }); 

  test('Signing in', () async {
    AuthService _auth = AuthService();
    String email = "cata@yahoo.com";
    String password = "parola1234";
    dynamic result = await _auth.signInWithEmailAndPassword(email, password);
    expect(result, null);
  });

  test('Register', () async {
    AuthService _auth = AuthService();
    String username = "cata";
    String email = "cata@yahoo.com";
    String password = "parola1234";
    dynamic result = await _auth.registerWithEmailAndPassword(username, email, password);
    expect(result, null);
  });

  test('Sign Out', () async{
    AuthService _auth = AuthService();
    dynamic result = await _auth.signOut();
    expect(result, null);
  });

  test('getData returns a list of <Review>', () {
    double amount1 = 5000;
    double amount2 = 3000;
    List<Review> expectedData = [
      Review(
        name: "Total",
        amount: amount1,
        barColor: charts.ColorUtil.fromDartColor(Colors.blue),
      ),
      Review(
        name: "Current",
        amount: amount2,
        barColor: charts.ColorUtil.fromDartColor(Colors.blue),
      )
    ];
    List<Review> result = getData(amount1, amount2);  
    expect(result.toString(), expectedData.toString());

  });
}