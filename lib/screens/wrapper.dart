import 'package:financeapp_mds/models/user.dart';
import 'package:financeapp_mds/screens/authenticate/authenticate.dart';
import 'package:financeapp_mds/screens/home/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:financeapp_mds/services/database.dart';
import 'package:financeapp_mds/providers/transactionProvider.dart';
import 'package:financeapp_mds/models/transaction.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final user = Provider.of<User>(context);


    // if (user == null) {
    //   return Authenticate();
    // }
    // else 
    // return MultiProvider(
    //   providers: [
    //     ChangeNotifierProvider(create: (context) => TransactionProvider()),
    //     StreamProvider(create: (context) => databaseService.getTransactions()),
    //   ],
    //   child: Home()
    // );

            // StreamProvider(create: (context) => AuthService().user)

    // return either the Home or Authenticate widget
    if (user == null){
      return Authenticate();
    } else {
        final databaseService = DatabaseService(uid: user.uid);

      // return StreamProvider<List<Transaction2>>.value(
      //   value: databaseService.getTransactions(),
      //   child: Home()
      // );
      return MultiProvider(
      providers: [
        
        StreamProvider(create: (context) => databaseService.getTransactions()),
        StreamProvider(create: (context) => databaseService.userData),
      ],
      child: Home()
      );
    }
    
    // if (user == null){
    //     return Authenticate();
    //   } else {
    //     return Home();
    //   }
    

  }
}