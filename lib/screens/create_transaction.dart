import 'package:financeapp_mds/services/database.dart';
import 'package:flutter/material.dart';
import 'package:financeapp_mds/shared/constants.dart';
import 'package:financeapp_mds/shared/loading.dart';
import 'package:financeapp_mds/models/transaction.dart';
import 'package:financeapp_mds/providers/transactionProvider.dart';
import 'package:provider/provider.dart';
import 'package:financeapp_mds/models/user.dart';

class CreateTransaction extends StatefulWidget {
  final Transaction2 transaction;

  CreateTransaction([this.transaction]);

  @override
  _CreateTransactionState createState() => _CreateTransactionState();
}

class _CreateTransactionState extends State<CreateTransaction> {

  final nameController = TextEditingController();
  final dateController = TextEditingController();
  final labelController = TextEditingController();
  final amountController = TextEditingController();

  String error = "";

  double initialAmount = 0;
  String initialLabel = "";

  @override
  void dispose() {
    nameController.dispose();
    dateController.dispose();
    labelController.dispose();
    amountController.dispose();
    super.dispose();
  }

  @override
  void initState() {

     if (widget.transaction == null) {
      //New Record
      nameController.text = "";
      dateController.text = "";
      labelController.text = Expenses.regularExpenses;
      amountController.text = "";

      new Future.delayed(Duration.zero, () {
        final transactionProvider = Provider.of<TransactionProvider>(context,listen: false);
        User user = Provider.of<User>(context);
        transactionProvider.loadValues(Transaction2());
        transactionProvider.changeUid(user.uid);

      });
    } else {
      //Controller Update
      nameController.text = widget.transaction.name;
      dateController.text = widget.transaction.date;
      labelController.text = widget.transaction.label;
      amountController.text = widget.transaction.amount.toString();
      initialAmount = widget.transaction.amount;
      initialLabel = widget.transaction.label;
      //State Update
      new Future.delayed(Duration.zero, () {
        final transactionProvider = Provider.of<TransactionProvider>(context,listen: false);
        transactionProvider.loadValues(widget.transaction);
      });

    }

    super.initState();

  }

  
  @override
  Widget build(BuildContext context) {
    final transactionProvider = Provider.of<TransactionProvider>(context);
    User user = Provider.of<User>(context);

    // final userData = Provider.of<UserData>(context);
    
    return StreamBuilder<UserData>(
      stream: DatabaseService(uid: user.uid).userData,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          UserData userData = snapshot.data;
        return Scaffold(
          appBar: AppBar(
              title: Text('Create a transaction'),
              backgroundColor: HexColor("#4A69FF"),
            ),
          resizeToAvoidBottomPadding: false,
          backgroundColor: Colors.white,
          body: Center(
            child: Container(
              decoration: BoxDecoration(
                boxShadow: [
                  new BoxShadow(
                    color: Colors.grey[300],
                    offset: new Offset(0, 0),
                    blurRadius: 15,
                    spreadRadius: 10
                  )
                ],
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10))
              ),
              width: 9 * MediaQuery.of(context).size.width / 10,
              height: 2.4 * MediaQuery.of(context).size.height / 3,
              padding: EdgeInsets.fromLTRB(30, 24, 30, 0),
              child: Column(
                
                children: <Widget>[
                  SizedBox(
                    height:44,
                    child: TextFormField(
                      controller: nameController,
                      errorTextPresent: false,
                      decoration: InputDecoration(  
                        hintText: 'Transaction name',
                        hintStyle:  TextStyle(
                              color: HexColor("#1A1A1A").withOpacity(0.6),
                              fontSize: 14,
                              height: 0.85,
                            ),
                        enabledBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(40.0),
                        borderSide: BorderSide(color: HexColor("#1A1A1A").withOpacity(0.2)),
                        ),
                        
                        border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(40.0),
                        borderSide: BorderSide(color: HexColor("#1A1A1A").withOpacity(0.2)),

                        ),
                      ),
                      

                      onChanged: (val) {
                        transactionProvider.changeName(val);
                      },
                    ),
                  ),
                    SizedBox(height: 16.0),
                    SizedBox(
                      height: 44,
                      child: TextFormField(
                        controller: dateController,
                        errorTextPresent: false,
                        decoration: InputDecoration(
                          hintText: 'Date',
                          hintStyle:  TextStyle(
                                color: HexColor("#1A1A1A").withOpacity(0.6),
                                fontSize: 14,
                                height: 0.85,

                              ),
                          enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(40.0),
                          borderSide: BorderSide(color: HexColor("#1A1A1A").withOpacity(0.2)),
                          ),
                          
                          border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(40.0),
                          borderSide: BorderSide(color: HexColor("#1A1A1A").withOpacity(0.2)),

                          ),
                        ),
                        

                        onChanged: (val) {
                          transactionProvider.changeDate(val);
                        },
                      ),
                    ),
                    SizedBox(height: 16.0),

                    SizedBox(
                      height: 44,
                      child: TextFormField(
                        controller: amountController,
                        errorTextPresent: false,
                        decoration: InputDecoration( 
                          hintText: 'Amount',
                          hintStyle:  TextStyle(
                                color: HexColor("#1A1A1A").withOpacity(0.6),
                                fontSize: 14,
                                height:0.8,
                                
                              ),
                          enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(40.0),
                          borderSide: BorderSide(color: HexColor("#1A1A1A").withOpacity(0.2)),

                          ),
                          border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(40.0),
                          borderSide: BorderSide(color: HexColor("#1A1A1A").withOpacity(0.2)),

                          ),
                        ),
                        
                        
                        onChanged: (val) {
                        transactionProvider.changeAmount(val);

                        }
                      ),
                    ),
                    SizedBox(height:16),
                  
                    Row(
                      children: <Widget>[
                        SizedBox(width:10),
                        Text(
                          "Label",
                          style: TextStyle(
                                color: HexColor("#1A1A1A").withOpacity(0.6),
                                fontSize: 14,
                                
                              ),
                          ),
                        SizedBox(width:10),
                        DropdownButton<String>(
                          
                          value: labelController.text ?? transactionProvider.label,
                          icon: Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 16,
                          style: TextStyle(color: HexColor("#8A9EAD")),
                          
                          underline: Container(
                            height: 2,
                            color: HexColor("#4A69FF"),

                          ),
                          onChanged: (String newValue) {
                            labelController.text = newValue;
                            transactionProvider.changeLabel(newValue);


                          },
                          items: <String>[Expenses.regularExpenses, Expenses.personalExpenses, Expenses.investment]
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ) ,
                      ],
                    ),

                    SizedBox(height: 40.0),
                    SizedBox(
                      height: 44,
                      width: 130,
                      child: RaisedButton(
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(25.0)
                        ),
                        color: HexColor("#4A69FF"),
                        child: Text(
                          'Save',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () async {
                          setState(() {
                            if (error != "")
                            error = "";
                          if (transactionProvider.name == null) {
                            error += "Enter a name \n";
                          }
                          if (transactionProvider.date == null) {
                            error += "Enter a date \n";
                          }
                          if (transactionProvider.amount == null) {
                            error += "Enter an amount \n";
                          }
                          if (transactionProvider.label == null) {
                            error += "Select a label \n";
                          }
                          });
                          

                          if (transactionProvider.label == Expenses.regularExpenses)
                            transactionProvider.saveTransaction(userData.currentBudget, userData.currentRegularExpenses, userData.currentInvestment, userData.currentPersonalExpenses, initialAmount, initialLabel);
                          else if (transactionProvider.label == Expenses.investment)
                            transactionProvider.saveTransaction(userData.currentBudget, userData.currentRegularExpenses, userData.currentInvestment, userData.currentPersonalExpenses, initialAmount, initialLabel);
                          else
                            transactionProvider.saveTransaction(userData.currentBudget, userData.currentRegularExpenses, userData.currentInvestment, userData.currentPersonalExpenses, initialAmount, initialLabel);

                          if (error == "")
                            Navigator.of(context).pop();
                         
                        
                        }
                      ),
                    ),

                    SizedBox(height:40),

                    (widget.transaction != null) ? SizedBox(
                      height: 44,
                      width: 130,
                      child: RaisedButton(
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(25.0)
                        ),
                        color: Colors.red,
                        child: Text(
                          'Delete',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          
                          if (transactionProvider.label == Expenses.regularExpenses)
                            transactionProvider.removeTransaction(widget.transaction.transactionId, userData.currentBudget, userData.currentRegularExpenses);
                          else if (transactionProvider.label == Expenses.investment)
                            transactionProvider.removeTransaction(widget.transaction.transactionId, userData.currentBudget, userData.currentInvestment);
                          else
                            transactionProvider.removeTransaction(widget.transaction.transactionId, userData.currentBudget, userData.currentPersonalExpenses);

                          Navigator.of(context).pop();
                        }
                      ),
                    ): Container(height:44),

                  SizedBox(height:12.0),
                  Text(
                    error,
                    style: TextStyle(color: Colors.red, fontSize: 14.0),
                  )
                    
                  ],
                        )
                      ),
            ),
          );
      }
      else
      return Loading(); 
      }
    );
  }
}