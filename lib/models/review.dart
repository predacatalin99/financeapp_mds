import 'package:charts_flutter/flutter.dart' as charts;

class Review {
  final String name;
  final double amount;
  final charts.Color barColor;
  Review({this.name, this.amount, this.barColor});
  

}