import 'package:flutter/material.dart';
import 'package:financeapp_mds/shared/constants.dart';
import 'package:financeapp_mds/models/review.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:financeapp_mds/widgets/review_chart.dart';
import 'package:provider/provider.dart';
import 'package:financeapp_mds/models/user.dart';
import 'package:financeapp_mds/services/database.dart';
import 'package:financeapp_mds/shared/loading.dart';
class WeeklyReview extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context);
    return StreamBuilder<UserData>(
      stream: DatabaseService(uid: user.uid).userData,
      builder: (context, snapshot) {
      if (snapshot.hasData) {
      return Scaffold(
      appBar: AppBar(
          title: Text('Financial review'),
          backgroundColor: HexColor("#4A69FF"),
        ),
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            boxShadow: [
              new BoxShadow(
                color: Colors.grey[300],
                offset: new Offset(0, 0),
                blurRadius: 15,
                spreadRadius: 10
              )
            ],
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          width: 9 * MediaQuery.of(context).size.width / 10,
          height: 2.4 * MediaQuery.of(context).size.height / 3,
          padding: EdgeInsets.fromLTRB(0, 24, 0, 0),
          child: Column(
            children: <Widget>[
            Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    ReviewChart(chartName: "Budget", data: getData(snapshot.data.totalBudget, snapshot.data.currentBudget)),
                     ReviewChart(chartName: "Regular expenses", data: getData(snapshot.data.totalBudget * snapshot.data.regularExpenses / 100, snapshot.data.currentRegularExpenses)),
                  ],
                ),
                Row(
                  children: <Widget>[
                    ReviewChart(chartName: "Investment", data: getData(snapshot.data.totalBudget * snapshot.data.investment / 100, snapshot.data.currentInvestment)),
                    ReviewChart(chartName: "Personal expenses", data: getData(snapshot.data.totalBudget * snapshot.data.personalExpenses/ 100, snapshot.data.currentPersonalExpenses)),
                    
                  ],
                ),
              ],
            ),
          ],)
          ),
      )
      );
      }
      else
        return Loading();
      }
      
    );
  }
}

List<Review> getData(double amount1, double amount2) {
  final List<Review> data = [
    Review(
      name: "Total",
      amount: amount1,
      barColor: charts.ColorUtil.fromDartColor(Colors.blue),
    ),
    Review(
      name: "Current",
      amount: amount2,
      barColor: charts.ColorUtil.fromDartColor(Colors.blue),
    )
  ];
  return data;

}