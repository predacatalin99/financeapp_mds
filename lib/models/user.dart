class User {

  final String uid;
  
  User({ this.uid });

}

class UserData {

  final String uid;
  final String username;
  final int regularExpenses;
  final int investment;
  final int personalExpenses;
  final double totalBudget;
  final double currentBudget;
  final double currentRegularExpenses;
  final double currentPersonalExpenses;
  final double currentInvestment;

  UserData({ this.uid, this.username, this.regularExpenses, this.investment, this.personalExpenses, this.totalBudget, this.currentBudget, this.currentRegularExpenses, this.currentPersonalExpenses, this.currentInvestment });

}

class Username {

  final String username;

  Username({ this.username });

}