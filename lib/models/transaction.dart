class Transaction2 {
  final String transactionId;
  final String name;
  final String date;
  final String label;
  final double amount;
  final String uid;
  Transaction2({this.uid, this.transactionId, this.name, this.date, this.label, this.amount});
  
  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'transactionId': transactionId,
      'name': name,
      'date': date,
      'label': label,
      'amount': amount,
    };
  }

  Transaction2.fromFirestore(Map<String, dynamic> firestore) 
    : uid = firestore['uid'],
      transactionId = firestore['transactionId'],
      name = firestore['name'],
      date = firestore['date'],
      label = firestore['label'],
      amount = firestore['amount'];
  
}