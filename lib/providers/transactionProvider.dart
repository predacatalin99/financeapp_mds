import 'package:financeapp_mds/models/transaction.dart';
import 'package:financeapp_mds/services/database.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class TransactionProvider with ChangeNotifier {
  var databaseService = DatabaseService();

  static String _uid;
  String _name;
  String _date;
  String _label;
  double _amount;
  String _transactionId;
  var uuid = Uuid();

  String get name => _name;
  String get date => _date;
  String get label => _label;
  double get amount => _amount;
  String get uid => _uid;

  changeName(String value) {
    _name = value;
    notifyListeners();
  }

  changeDate(String value) {
    _date = value;
    notifyListeners();
  }

  changeLabel(String value) {
    _label = value;
    notifyListeners();
  }

  changeAmount(String value) {
    _amount = double.parse(value);
    notifyListeners();
  }

  changeUid(String value) {

    _uid = value;
    databaseService = DatabaseService(uid: _uid);
    notifyListeners();
  }


  loadValues(Transaction2 transaction) {
    _uid = transaction.uid;
    _transactionId = transaction.transactionId;
    _name = transaction.name;
    _date = transaction.date;
    _label = transaction.label;
    _amount = transaction.amount;
  }

  saveTransaction(double currentBudget, double currentRegularExpenses, double currentInvestment, double currentPersonalExpenses, double initialTransactionAmount, String initialLabel) {
    if (name == null || date == null || amount == null || label == null)
      return;
    print(_uid);
    if (_transactionId == null) {
      var newTransaction = Transaction2(name: name, date: date, label: label, amount: amount, transactionId: uuid.v4(), uid: _uid);
      databaseService.saveTransaction(newTransaction, currentBudget, currentRegularExpenses, currentInvestment, currentPersonalExpenses, initialTransactionAmount, initialLabel);
    }
    else {
      var updatedTransaction = Transaction2(name: name, date: date, label: label, amount: amount, transactionId: _transactionId, uid: _uid);
      databaseService.saveTransaction(updatedTransaction, currentBudget, currentRegularExpenses, currentInvestment, currentPersonalExpenses, initialTransactionAmount, initialLabel);
    }
  }

  removeTransaction(String transactionId,  double initialBudget, double initialValue) {
    databaseService.removeTransaction(transactionId, label, initialBudget, initialValue, amount);
  }

}