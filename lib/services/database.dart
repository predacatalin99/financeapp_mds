import 'package:financeapp_mds/models/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:financeapp_mds/models/transaction.dart';
import 'package:financeapp_mds/shared/constants.dart';

class DatabaseService {

  final String uid;
  DatabaseService({ this.uid });

  // collection reference
  final CollectionReference userCollection = Firestore.instance.collection('users');
  final CollectionReference transactionCollection = Firestore.instance.collection('transactions');


  Future<void> saveTransaction(Transaction2 transaction, double initialBudget, double initialRegularExpenses, double initialInvestment, double initialPersonalExpenses, double initialTransactionAmount, String initialLabel) async{

    if (initialLabel == Expenses.regularExpenses) {
      initialRegularExpenses += initialTransactionAmount;
    }
    else if (initialLabel == Expenses.investment) {
      initialInvestment += initialTransactionAmount;
    }
    if (initialLabel == Expenses.personalExpenses) {
      initialPersonalExpenses += initialTransactionAmount;
    }

    initialBudget += initialTransactionAmount - transaction.amount;

    if (transaction.label == Expenses.regularExpenses) {
      initialRegularExpenses -= transaction.amount;
    }
    else if (transaction.label == Expenses.investment) {
      initialInvestment -= transaction.amount;
    }
    else if (transaction.label == Expenses.personalExpenses) {
      initialPersonalExpenses -= transaction.amount;
    }

    await userCollection.document(uid).updateData({
      'currentBudget': initialBudget,
      'currentRegularExpenses': initialRegularExpenses,
      'currentInvestment': initialInvestment,
      'currentPersonalExpenses': initialPersonalExpenses,
    });

    return transactionCollection.document(transaction.transactionId).setData(transaction.toMap());
  }

  Stream<List<Transaction2>> getTransactions() {

    return transactionCollection.snapshots().map((snapshot) => snapshot.documents.map((document) => Transaction2.fromFirestore(document.data)).where((t) => t.uid == this.uid).toList());
    // return transactionCollection.snapshots().map((snapshot) => snapshot.documents.map((document) => Transaction2.fromFirestore(document.data)).toList());

  }

  Future<void> removeTransaction(String transactionId, String label, double initialBudget, double initialValue, double transactionAmount) async{

    initialBudget += transactionAmount;
    initialValue += transactionAmount;

    if (label == Expenses.regularExpenses) {
      await userCollection.document(uid).updateData({
        'currentBudget': initialBudget,
        'currentRegularExpenses': initialValue,
      });
    }
    else if (label == Expenses.investment) {
      await userCollection.document(uid).updateData({
        'currentBudget': initialBudget,
        'currentInvestment': initialValue,
      });
    }
    else if (label == Expenses.personalExpenses){
      await userCollection.document(uid).updateData({
        'currentBudget': initialBudget,
        'currentPersonalExpenses': initialValue,
      });
    }  

    return transactionCollection.document(transactionId).delete();
  }

  Future<void> updateUserData(String username, int regularExpenses, int investment, int personalExpenses, double totalBudget, double currentBudget, double currentRegularExpenses, double currentInvestment, double currentPersonalExpenses) async {
    return await userCollection.document(uid).setData({
      'username': username,
      'regularExpenses': regularExpenses,
      'investment': investment,
      'personalExpenses': personalExpenses,
      'totalBudget': totalBudget,
      'currentBudget': currentBudget,
      'currentRegularExpenses': currentRegularExpenses,
      'currentInvestment': currentInvestment,
      'currentPersonalExpenses': currentPersonalExpenses,

    });
  }


  // user data from snapshots
  UserData _userDataFromSnapshot(DocumentSnapshot snapshot) {
    return UserData(
      uid: uid,
      username: snapshot.data['username'],
      regularExpenses: snapshot.data['regularExpenses'],
      investment: snapshot.data['investment'],
      personalExpenses: snapshot.data['personalExpenses'],
      totalBudget: snapshot.data['totalBudget'],
      currentBudget: snapshot.data['currentBudget'],
      currentRegularExpenses: snapshot.data['currentRegularExpenses'],
      currentInvestment: snapshot.data['currentInvestment'],
      currentPersonalExpenses: snapshot.data['currentPersonalExpenses'],

    );
  }


  // get user doc stream
  Stream<UserData> get userData {
    return userCollection.document(uid).snapshots()
      .map(_userDataFromSnapshot);
  }

}