import 'package:financeapp_mds/screens/wrapper.dart';
import 'package:financeapp_mds/services/auth.dart';
import 'package:financeapp_mds/services/database.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:financeapp_mds/models/user.dart';
import 'package:financeapp_mds/providers/transactionProvider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final databaseService = DatabaseService();

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => TransactionProvider()),
        StreamProvider(create: (context) => databaseService.getTransactions()),
        StreamProvider(create: (context) => AuthService().user)
      ],
      child: MaterialApp(
        home: Wrapper(),
      ),
    );


    // return StreamProvider<User>.value(
    //   value: AuthService().user,
    //   child: MaterialApp(
    //     home: Wrapper(),
    //   ),
    // );
  }
}