import 'package:flutter/material.dart';
import 'package:financeapp_mds/models/transaction.dart';
import 'package:financeapp_mds/shared/constants.dart';

class TransactionItem extends StatelessWidget {
  final Transaction2 transaction;
  final Function onDeleteItem;
  const TransactionItem({
    Key key, 
    this.transaction,
    this.onDeleteItem,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      margin: const EdgeInsets.only(top: 10),
      // alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                  child: Padding(
                padding: EdgeInsets.only(left:15),
                child: Text(
                  this.transaction.name,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                )
              ),
              Padding(
                padding: const EdgeInsets.only(right:10),
                child: Row(children: <Widget>[
                  Text(
                  "-",
                  style:TextStyle(fontSize: 30, color: HexColor("#4AD285"),),
                ),
                Text(
                  this.transaction.amount.toString(),
                  style:TextStyle(color: HexColor("#4AD285"),)
                ),
                Text(
                  " RON",
                  style:TextStyle(color: HexColor("#4AD285"),)
                ),
                ],),
              )
              
            ],
          ),
          Row(
            children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.only(left:15.0),
                  child: Text(
                    this.transaction.date,
                    style: TextStyle(
                      color: HexColor("#1A1A1A").withOpacity(0.4),
                      fontSize: 14,
                      height: 0.85,
                    ),
                  )
                )
              ),
              SizedBox(width: 10),
              Text(
                this.transaction.label,
                style:TextStyle(height:0.85)
                )
            ],
          )
        ],
      ),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
              new BoxShadow(
                color: Colors.grey[300],
                offset: new Offset(0, 0),
                blurRadius: 15,
                spreadRadius: 10
              )
            ],
    )
    );
  }
}