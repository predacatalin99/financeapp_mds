import 'package:financeapp_mds/models/transaction.dart';
import 'package:financeapp_mds/services/auth.dart';
import 'package:flutter/material.dart';
import 'package:financeapp_mds/shared/constants.dart';
import 'package:provider/provider.dart';
import 'package:financeapp_mds/models/user.dart';
import 'package:financeapp_mds/services/database.dart';
import 'package:financeapp_mds/shared/loading.dart';
import 'package:financeapp_mds/screens/financialFormula.dart';
import 'package:financeapp_mds/widgets/transaction_item.dart';
import 'package:financeapp_mds/screens/create_transaction.dart';
import 'package:financeapp_mds/screens/financial_review.dart';

class Home extends StatelessWidget {

  final AuthService _auth = AuthService();
 // final List<Transaction2> transactions = getTransactions();



  @override
  Widget build(BuildContext context) {


    final transactions = Provider.of<List<Transaction2>>(context);
    User user = Provider.of<User>(context);

    return StreamBuilder<UserData>(
      stream: DatabaseService(uid: user.uid).userData,
      builder: (context, snapshot) {
        if(snapshot.hasData){
          UserData userData = snapshot.data;
      return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('Savinator'),
          backgroundColor: HexColor("#4A69FF"),

        ),
        drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text(
                '${userData.username}',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                )
              ),
              decoration: BoxDecoration(
                image: new DecorationImage(
                  image: new NetworkImage("https://img00.deviantart.net/35f0/i/2015/018/2/6/low_poly_landscape__the_river_cut_by_bv_designs-d8eib00.jpg"),
                  fit: BoxFit.fill
                )
              ),
            ),
            ListTile(
              title: Text('Logout'),
              trailing: Icon(Icons.person),

              onTap: () async {
                await _auth.signOut();
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Financial Formulas'),
              trailing: Icon(Icons.settings),

              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => FinancialFormula()),
                );
              },
            ),
            ListTile(
              title: Text('Financial Review'),
              trailing: Icon(IconData(0xe935, fontFamily: 'MaterialIcons')),

              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => WeeklyReview()),
                );
              },
            ),
            new Divider(),
            new ListTile(
              title: new Text("Cancel"),
              trailing: new Icon(Icons.cancel),
              onTap: () => Navigator.pop(context),
            ),
          ],
        ),
      ),
        body: (transactions != null) ? Container(
          child: Column(
            children: <Widget>[
              SizedBox(height:10),
              Text("Your total budget: ${userData.totalBudget} RON", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
              Text("Your current budget: ${userData.currentBudget} RON", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
              Text("Your current regular expenses: ${userData.currentRegularExpenses} RON", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
              Text("Your current investment: ${userData.currentInvestment} RON", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
              Text("Your current personal expenses: ${userData.currentPersonalExpenses} RON", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
              
              Container(
                child: Expanded(
                  child: ListView.builder(
                    padding: EdgeInsets.zero,

                    itemCount:transactions.length,
                    itemBuilder: (context, index) =>
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => CreateTransaction(transactions[index])),
                        );
                      },
                      child: TransactionItem(
                        transaction: transactions[index],
                        onDeleteItem: () {},
                      ),
                    ),
                  ),
                ),
              ),
            ],
          )
          
        ) : Loading(),
        floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CreateTransaction()),
          );
        },
        child: Icon(Icons.add),
        backgroundColor: HexColor("#4A69FF"),
      ),
      );
      }
      else
        return Loading();
      }
    );
  }
}

// List<Transaction> getTransactions() {
//   List<Transaction> transactions = [];
//   Transaction transaction = Transaction(uid:"Uid", name:"Transaction name", date:"June 5 3:00PM", label:"Regular expenses", amount:2000);
//   for (int i = 1; i <= 10; i++)
//     transactions.add(transaction);
//   return transactions;
// }
